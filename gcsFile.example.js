/**
* Configuration for gcsFile module
**/

module.exports = {
  /**
  * Objects with the options for each model atribute representing a file.
  */
  modelFiles: {
  },

  /**
  * Defined file types with options
  **/
  types: {
  },

  /**
  * Posible functions to run before upload
  */

  tasks: {
  },

  /**
  * Default Upload Options
  */
  defaultUploadOptions: {
    public: true,
    metadata: {
      cacheControl: 'private'
    }
  },

  /**
  * Base url for storing files
  */
  storageUrl: '',

  /**
  * Gets the bucket name
  */
  getBucketName: function() {
  },

  /**
  * Gets the path for the given object
  **/
  getPath: function(gcsObject) {
  },

  /**
  * Gets the cloud module to upload an delete files
  */
  getCould: function() {
    return {
      upload: function() {},
      delete: function() {}

    };
  }

};
