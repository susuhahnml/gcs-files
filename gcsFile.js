/**
* Configuration for files stored in cloud
**/
const GCSFileError = require('./lib/GCSFileError');

module.exports = {
  modelFiles: {
    Banner: {
      desktop_picture: {
        type: 'pngImage',
        options: {
          width: 952,
          height: 596
        },
      },
      mobile_picture: {
        type: 'pngImage',
        options: {
          width: 385,
          height: 241
        },
      },
      gallery: {
        type: 'pngImage',
        options: {
          width: 790,
          height: 500
        },
      },
      privacy_notice: {
        type: 'pdf',
      },
    },
  },

  types: {
    pngImage: {
      ext: '.png',
      acceptedMimes: [
        'image/jpeg',
        'image/jpg',
        'image/png'
      ],
      mimeType: 'image/png',
      encodingFormat: 'png',
      encodingCompresionRate: 85,
      beforeUpload: ['validateNotBiggerSize', 'validateHasUploadedData', 'validateExtension', 'validateNotSmallerDimentions', 'validateSameProportion', 'encode']
    },
    pdf: {
      ext: '.pdf',
      acceptedMimes: ['application/pdf'],
      mimeType: 'application/pdf',
      beforeUpload: ['validateNotBiggerSize', 'validateHasUploadedData', 'validateExtension']
    }
  },

  tasks: {
    validateWidthPair: function() {
      throw new GCSFileError('invalidWidth', 'InvalidWidth', 'invalidWidth');
    }
  },

  defaultUploadOptions: {
    public: true,
    metadata: {
      cacheControl: 'private'
    }
  },

  maxSize: '1mb', //Hardcoded directly in config/http.js

  storageUrl: 'https://storage.googleapis.com',

  getBucketName: function() {
    return 'abc-web';
  },

  getPath: function(gcsObject) {
    const slug = gcsObject.extraOptions.brand.slug;
    const modelNamePlural = gcsObject.modelInstance._modelOptions.name.plural.toLowerCase();
    const modelId = gcsObject.modelInstance.id;
    const fileName = gcsObject.fileName;
    const extension = gcsObject.type.ext;

    return slug + '/' + modelNamePlural + '/' + modelId + '/' + fileName + extension;
  },

  getCould: function() {
    return {
      upload: function() {},
      delete: function() {}

    };
  }

};
