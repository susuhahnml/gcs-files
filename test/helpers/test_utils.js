'use strict';

module.exports = {
  /**
  * Used in tests instead of busboy used in http as middleware
  */
  bufferToFileBusboy: function(buffer, mimetype, exededSize){
    return {
      data: buffer,
      mimetype: mimetype,
      truncated: exededSize,
      name: 'testGenerated.png'
    }
  }

};
