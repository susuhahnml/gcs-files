const debug = require('debug')('bootstrap');
const should = require('should');


/**
* Using Sequelize
**/
const db = require('./db');

before(() => {
  return db.sync()
  .then(() => {
    global.should = should;
    /**
    * Using Sequelize
    **/
    const FactoryGirl = require('factory-girl');
    const path = require('path');
    const requireTree = require('require-tree');
    const factory = FactoryGirl.factory;
    const adapter = new FactoryGirl.SequelizeAdapter();
    const folder = path.join(process.cwd(), 'test', 'factories');
    const factories = requireTree(folder);
    factory.setAdapter(adapter);


    Object.keys(factories).forEach(key => {
      factories[key](factory);
    });
    factory.models = models;
    global.factory = factory;
  })
  .catch((err) => {
    debug("Databse not syncronized");
    debug(err);
    throw err;
  })
});

afterEach(() => {
  /**
  * Clean environment before each test
  */
  return db.drop()
  .then(() => {
    return db.sync();
  })
});
